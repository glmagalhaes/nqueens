/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package nqueens;

import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Window7
 */
public class Tabuleiro {
    private final List<Rainha> rainhas;
    int n;
    
    public Tabuleiro(int n) throws BoardTooSmallException{
        if(n<1){
            throw new BoardTooSmallException();
        }
        this.n = n;
        this.rainhas = new LinkedList<>();
    }
    
    
    @Override
    public String toString(){
        //TODO: Implementar
        String tabuleiro = new String();
        for(int i=0; i<this.n; i++)
        {
            tabuleiro += this.rainhas.get(i).toString(this.n);
        }
        //tabuleiro+="\n";
        return tabuleiro;
    }
    
    public Tabuleiro(int n , Rainha primeira_rainha) throws BoardTooSmallException{
        if(n<1){
            throw new BoardTooSmallException();
        }
        this.n = n;
        this.rainhas = new LinkedList<>();
        this.rainhas.add(primeira_rainha);
              
    }
    
    public boolean Check(){
        return this.rainhas.size()==this.n;
    }
    
    public void moveLastQueenRight() throws nqueens.OutOfBoundsException{
        Rainha ultima_rainha = rainhas.get(rainhas.size()-1);
        rainhas.remove(rainhas.size()-1);
        ultima_rainha.move_right();
        boolean ataca;
        
        do {
            ataca = false;
            if (ultima_rainha.get_x() == n){
                throw new OutOfBoundsException();
            }
            for(int i = 0; i< this.rainhas.size();i++){
                int x1,x2,y1,y2;
                x1 = this.rainhas.get(i).get_x();
                x2 = ultima_rainha.get_x();
                y1 = this.rainhas.get(i).get_y();
                y2 = ultima_rainha.get_y();
                //TODO: Test if x1 = x2 or y2 = y2 or |x1-x2| == |y1-y2|
                if((x1 == x2) || (y1 == y2)){
                    ataca = true;
                    ultima_rainha.move_right();
                    break;
                } else if (Math.abs(x1-x2) == Math.abs(y1-y2)) {
                    ataca = true;
                    ultima_rainha.move_right();
                    break;
                }
            }
        }while(ataca);
        
        rainhas.add(ultima_rainha);
    }
        
    /**
     *
     * @throws FullBoardException
     * @throws nqueens.OutOfBoundsException
     */
    public void addQueen() throws FullBoardException, OutOfBoundsException{
        if (this.n == this.rainhas.size()){
            //TODO: Raise Full Board Exception
            throw new FullBoardException();
        }
        Rainha nova_rainha;
        Rainha ultima_rainha = rainhas.get(rainhas.size()-1);
        nova_rainha = new Rainha(0,ultima_rainha.get_y()+1);
        
        boolean ataca;
        
        do {
            ataca = false;
            if (nova_rainha.get_x() == n){
                throw new OutOfBoundsException();
            }
            for(int i = 0; i< this.rainhas.size();i++){
                int x1,x2,y1,y2;
                x1 = this.rainhas.get(i).get_x();
                x2 = nova_rainha.get_x();
                y1 = this.rainhas.get(i).get_y();
                y2 = nova_rainha.get_y();
                //TODO: Test if x1 = x2 or y2 = y2 or |x1-x2| == |y1-y2|
                if((x1 == x2) || (y1 == y2)){
                    ataca = true;
                    nova_rainha.move_right();
                    break;
                } else if (Math.abs(x1-x2) == Math.abs(y1-y2)) {
                    ataca = true;
                    nova_rainha.move_right();
                    break;
                }
            }
        }while(ataca);
        
        rainhas.add(nova_rainha);
    }
    
    public int numberOfQueens(){
        return this.rainhas.size();
    }

    private void OutOfBoundsException() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
