/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package nqueens;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Window7
 */
public class NQueens {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int n;
        
        n = args.length > 0 ? Integer.parseInt(args[0]) : 8;
		
		if(n == 1){
			System.out.println("R");
			return;
		}
		
        for(int i=0; i<n; i++)
        {
            Tabuleiro tab;
            try {
                tab = new Tabuleiro(n, new Rainha(i,0));
            } catch (BoardTooSmallException ex) {
                Logger.getLogger(NQueens.class.getName()).log(Level.INFO, "Invalid Board or Board is smaller than 1.", ex);
                System.out.println("Invalid Board or Board is smaller than 1.");
                break;
            }
            Jogador jog = new Jogador(tab);
            jog.start();
        }
    }
}
