/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package nqueens;

/**
 *
 * @author Window7
 */
public class Rainha {
    private int x,y;
    
   public Rainha(int x, int y){
       this.x = x;
       this.y = y;
   }

    Rainha() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
   public int get_x(){
       return this.x;
   }
   public int get_y(){
       return this.y;
   }
   public void move_right(){
       this.x +=1;
   }
   @Override
   public String toString(){
       String string = "(" + this.x + ";" + this.y + ") ";
       return string;
   }
   public String toString(int n){
       String string = "";
       for (int i = 0; i < n; i++) {
           string += i == this.x ? "R ": "X "; 
       }
       string += "\n";
       return string;
   }
}
