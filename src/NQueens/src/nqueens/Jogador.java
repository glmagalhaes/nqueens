/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nqueens;

import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author Window7
 */
public class Jogador extends Thread{

    Tabuleiro tabuleiro;

    public Jogador(Tabuleiro tabuleiro) {
        this.tabuleiro = tabuleiro;
    }

    public void resolveTodas() {
        while (true) {
            try {
                this.tabuleiro.addQueen();
            } catch (FullBoardException ex) {
                Logger.getLogger(Jogador.class.getName()).log(Level.FINE, null, ex);
                System.out.println(this.tabuleiro.toString());
                while (true) {
                    try {
                        this.tabuleiro.moveLastQueenRight();
                        break;
                    } catch (OutOfBoundsException ex1) {
                        Logger.getLogger(Jogador.class.getName()).log(Level.FINE, null, ex1);
                        if (this.tabuleiro.numberOfQueens() == 1) {
                            return;
                        }
                    }
                }
            } catch (OutOfBoundsException ex) {
                Logger.getLogger(Jogador.class.getName()).log(Level.FINE, null, ex);
                if (this.tabuleiro.numberOfQueens() == 1) {
                    return;
                } else {
                    while (true) {
                        try {
                            this.tabuleiro.moveLastQueenRight();
                            break;
                        } catch (OutOfBoundsException ex1) {
                            Logger.getLogger(Jogador.class.getName()).log(Level.FINE, null, ex1);
                            if (this.tabuleiro.numberOfQueens() == 1) {
                                return;
                            }
                        }
                    }
                }
            }
        }
    }

    @Override
    public void run() {
        this.resolveTodas();
    }
}
