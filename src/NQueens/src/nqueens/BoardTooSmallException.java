/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package nqueens;

/**
 *
 * @author Window7
 */
public class BoardTooSmallException extends Exception {

    /**
     * Creates a new instance of <code>BoardTooSmallException</code> without
     * detail message.
     */
    public BoardTooSmallException() {
    }

    /**
     * Constructs an instance of <code>BoardTooSmallException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public BoardTooSmallException(String msg) {
        super(msg);
    }
}
